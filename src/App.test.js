import { render, screen } from '@testing-library/react';
import App from './App';
import { Provider } from 'react-redux';
import store from './redux/store';
import {
  BrowserRouter as Router,
} from "react-router-dom";

test('renders learn react link', () => {
  render(<Router basename="/webapi-frontend" ><Provider store={store}><App /></Provider></Router>);
  // const linkElement = screen.getByText(/learn react/i);
  // expect(linkElement).toBeInTheDocument();
});
