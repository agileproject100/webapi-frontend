
import React from 'react';
import {
  Switch,
  Route,
  Link,
  withRouter,
} from "react-router-dom";
import {
  Nav,
  Navbar,
  NavItem,
  NavLink,
  NavbarBrand,
  NavbarText,
} from "reactstrap";
import configData from "./config/manifest.json";
import LoginPage from "./page/LoginPage";
import HomePage from "./page/HomePage";
import FavourtePage from "./page/FavourtePage";
import OrderPage from "./page/OrderPage";
import ProductPage from "./page/ProductPage";
import RegisterPage from "./page/RegisterPage"; 
import ProfilePage from "./page/ProfilePage"; 
import {getUserstatus , setUserstatus} from './redux/actions';
import { connect } from 'react-redux';
import VerifyPage from "./page/VerifyPage"; 


class App extends React.Component { 
  constructor(props) {
    super(props);

    this.state = {
      isLogin : false
    };
  }

  

  componentDidMount() {
    fetch(
      configData.server_url+"/loginStatus", 
      {
          method: 'GET', 
          credentials: 'include',
          headers: new Headers({
              'Content-Type': 'application/json'
          })
      }
    ).then(res => {
      if(res.ok) {
        return res.json();
      }

      throw new Error('');

    })
    .then((result) => {
          if(result !== null && result !== undefined){
            this.setState({
              isLogin : true
            });

            this.props.SET_USER({
              uid : result.uid,
              userName : result.userName
            });

            this.props.history.push('/home');
          }
      },
          (error) => {
              this.props.history.push('/login');
              this.setState({
                isLogin : false
              });
          }
      )
  }

  render() {
    return (
        <div className="App">
            <Navbar color="light" light expand="md">
            <NavbarBrand href="/">Cloth Shop</NavbarBrand>
              <Nav className="ml-auto" navbar>
                <NavItem>
                  <NavLink ><Link to="/home"  className="nav-link">Home</Link></NavLink>
                </NavItem>
                <NavItem>
                  <NavLink ><Link to="/favourte"  className="nav-link">Favourte</Link></NavLink>
                </NavItem>
                <NavItem>
                  <NavLink ><Link to="/order"  className="nav-link">Your Order</Link></NavLink>
                </NavItem>
                {this.props.uid === undefined || this.props.uid === null ? 
                  <NavItem>
                    <NavLink ><Link to="/login"  className="nav-link">Sign In / Sing Up</Link></NavLink>
                  </NavItem>
                  :
                  <>
                    <NavItem>
                      <NavLink ><Link to="/profile" className="nav-link">Profile</Link></NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink ><Link  onClick={this.onClickLogout} className="nav-link">Sign Out</Link></NavLink>
                    </NavItem>
                  </>
                }
              </Nav>
              {this.props.uid === undefined || this.props.uid === null  ? 
                <></>
                :
                <NavbarText>Hi {this.props.userName}</NavbarText>
              }
            </Navbar>
            
            <Switch>
              <Route path="/home">
                <HomePage />
              </Route>
              <Route path="/favourte">
                <FavourtePage />
              </Route>
              <Route path="/order">
                <OrderPage />
              </Route>
              <Route path="/product">
                <ProductPage />
              </Route>
              <Route path="/login">
                <LoginPage />
              </Route>
              <Route path="/register">
                <RegisterPage  />
              </Route>
              <Route path="/profile">
                <ProfilePage  />
              </Route>
              <Route path="/VerifyPage">
                <VerifyPage  />
              </Route>
            </Switch>
        </div>
    );
  }

  onClickLogout = () =>{
    fetch(
      configData.server_url+"/logout", 
      {
          method: 'GET', // or 'PUT'
          credentials: 'include',
          headers: new Headers({
              'Content-Type': 'application/json'
          })
      }
    ).then(res => {
      if(res.ok) {
        return res.json();
      }

      throw new Error('');

    })
    .then((result) => {
          if(result !== null && result !== undefined){
            this.props.SET_USER({
              uid : null,
              userName : null
            });
            alert(result.message);
            this.setState({
              isLogin : false
            });
            this.props.history.push('/login');
          }
      },
          (error) => {
              this.setState({
                isLogin : false
              });
          }
      )
  }

}

const mapStateToProps = (state) => {
  return {
      uid: state.uid,
      userName: state.userName
  }
};

const mapDispatchToProps = (dispatch, ownProps) => {

  return ({
    SET_USER: (user) => dispatch(setUserstatus(user)),
    GET_USER: () => dispatch(getUserstatus()),
  });
}


export default connect(mapStateToProps, mapDispatchToProps)(withRouter(App));
