import {GET_USER , SET_USER} from './actionType';

const initialState = {};

export default function reducer(state = initialState, action) {
    switch (action.type){
        case SET_USER:
            return action.payload
        case GET_USER:
            return {
                uid : state.uid,
                userNmae : state.userNmae
            };
        default:
            return state;
    }
} 