import {GET_USER , SET_USER} from './actionType';


export const setUserstatus = (user) => {
    return{
        type: SET_USER,
        payload: user
    };
}

export const getUserstatus = () => {
    return{
        type: GET_USER,
    };
}