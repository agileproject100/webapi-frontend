
import React from 'react';
import { Container , Col , FormGroup , Label , Input , Alert , Card , CardHeader , CardBody , CardFooter , Button , CardImg  , CardSubtitle , CardTitle , CardColumns } from 'reactstrap';
import { Formik , Form } from 'formik';
import configData from "../config/manifest.json";
import {
  withRouter,
} from "react-router-dom";

class HomePage extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        products : [],
        onloadData : false,
      }
    }

    componentDidMount(){

      fetch(
        configData.server_url+"/product", 
        {
            method: 'GET', // or 'PUT'
            credentials: 'include',
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        }
      ).then(res => res.json())
      .then((result) => {
            if(result.status === "S"){
              this.setState({
                products : result.products,
                onloadData : true,
              })
            }
        },
            (error) => {
                alert(error);
            }
        )

    }
   
    render() {
      return (
        <div>
          <Container>
            <Alert color="light">
              <h1>Home</h1>
            </Alert>
          </Container>
          <Container>
            <Formik
                initialValues={{ productName: '' }}
                validate={values => {
                  const errors = {};
                  
                  return errors;
                }}
                onSubmit={(values, { setSubmitting }) => {
                  this.setState({
                    onloadData : false,
                  });

                  var productName = values.productName !== undefined && values.productName !== '' ? values.productName : ""
                  var encodeuri = encodeURI(configData.server_url+"/product/"+productName);

                  fetch(
                    encodeuri, 
                    {
                        method: 'GET', // or 'PUT'
                        credentials: 'include',
                    }
                  ).then(res => res.json())
                  .then((result) => {
                        if(result.status === "S"){
                          this.setState({
                            products : result.products,
                            onloadData : true,
                          })
                        }
                        setSubmitting(false);
                    },
                        (error) => {
                            alert(error);
                            setSubmitting(false);
                        }
                    )
                }}
              >{({ values, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting, }) => (
                <Form> 
                  <Card>
                    <CardHeader>
                      <h3>Search Products</h3>
                    </CardHeader>
                    <CardBody>
                      <Col>
                      <FormGroup>
                        <Label for="productName">Product Name</Label>
                        <Input type="productName" name="productName" onChange={handleChange} onBlur={handleBlur}  placeholder="productName" />
                      </FormGroup>
                      </Col>
                    </CardBody>
                    <CardFooter>
                      <div className="float-right">
                        <Button color="primary" type="submit"  disabled={isSubmitting}>Search</Button>
                      </div>
                    </CardFooter>
                  </Card>
                </Form>
              )}
            </Formik>
          </Container>
          <br/>

          <Container>
            {this.state.onloadData ? 
              <CardColumns>
                {
                  this.state.products === [] ? <></> : 
                    this.state.products.map( (product) => {
                      return (
                        <Card>
                          <CardImg top width="100%" src={configData.server_url+"/colthImage/"+product.image} alt="Card image cap" />
                          <CardBody>
                            <CardTitle tag="h5">{product.productName}</CardTitle>
                            <CardSubtitle tag="h6" className="mb-2 text-muted">HKD$ {product.price}</CardSubtitle>
                            <Button onClick={(e) => this.onClickView(e,product)} color="info">View</Button>{'  '}
                          </CardBody>
                        </Card>
                      )
                    })
                }
              </CardColumns>
            :
            <div className="text-center">
              <div className="spinner-border" role="status">
                <span className="sr-only" >Loading...</span>
              </div>
            </div>
            }  
          </Container>
        </div>
      );
    }


    onClickView = (e,product) => {
      this.props.history.push('/product',product);
    }



  }


  export default withRouter(HomePage);