
import React from 'react';
import configData from "../config/manifest.json";
import { Formik , Form } from 'formik';
import { connect } from 'react-redux';
import { Container , Alert , Button  , Input , Label , Col , FormGroup , Card, CardBody, } from 'reactstrap';

class ProfilePage extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        orders : [],
        onloadData : false,
        totalPrice : 0,
      }
    }

    componentDidMount(){


      fetch(
        configData.server_url+"/order", 
        {
            method: 'GET', 
            credentials: 'include',
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        }
      ).then(res => res.json())
      .then((result) => {
            if(result.status === "S"){
              var totp = 0;
              for(var n = 0 ; n < result.orders.length ; n++){
                totp += totp + ( result.orders[n].quantity * result.orders[n].price )
              }
              this.setState({
                orders : result.orders,
                onloadData : true,
                totalPrice : totp,
              })
            }else if(result.status === "U"){
              alert(result.message);
              this.setState({
                onloadData : true,
              })
            }
        },
            (error) => {
                alert(error);
                this.setState({
                  onloadData : true,
                })
            }
        )
    }
   
    render() {
      return (
        <div>
          <Container>
            <Alert color="light">
              <h1>Profile</h1>
            </Alert>
          </Container>
          <Container fluid="sm">
            <Col sm="12" md={{ size: 8, offset: 2 }}>
              <Card >
                <CardBody>
                  <Formik
                    initialValues={{ userName: this.props.userName, oldPassword: '' , newPassword : '' }}
                    validate={values => {
                      const errors = {};
                      
                      if(values.oldPassword === '' || values.oldPassword === undefined){
                        errors.oldPassword = 'Please input the old password.'
                      }

                      if(values.newPassword === '' || values.newPassword === undefined){
                        errors.newPassword = 'Please input the new password.'
                      }

                      return errors;
                    }}
                    onSubmit={(values, { setSubmitting }) => {

                      fetch(
                        configData.server_url+"/profile", 
                        {
                            method: 'PUT', // or 'PUT'
                            body: JSON.stringify({oldPassword : values.oldPassword , newPassword : values.newPassword}), // data can be `string` or {object}!
                            credentials: 'include',
                            headers: new Headers({
                                'Content-Type': 'application/json'
                            })
                        }
                      ).then(res => res.json())
                      .then((result) => {
                            alert(result.message);
                            setSubmitting(false);
                        },
                            (error) => {
                                alert(error);
                                setSubmitting(false);
                            }
                        )


                    }}
                  >{({ values, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting, }) => (
                    <Form> 
                      <FormGroup>
                        <Label for="userName">User Name</Label>
                        <Input type="text" name="userName"  value={values.userName} disabled={true} />
                      </FormGroup>

                      <FormGroup>
                        <Label for="oldPassword">Old Passeord</Label>
                        <Input type="password" name="oldPassword" onChange={handleChange} onBlur={handleBlur} value={values.oldPassword} placeholder="oldPassword" />
                        <div style={{color:'red'}}>{errors.oldPassword && touched.oldPassword && errors.oldPassword}</div>
                      </FormGroup>
                      
                      <FormGroup>
                        <Label for="newPassword">New Password</Label>
                        <Input type="password" name="newPassword" onChange={handleChange} onBlur={handleBlur} value={values.newPassword} placeholder="newPassword " />
                        <div style={{color:'red'}}>{errors.newPassword && touched.newPassword && errors.newPassword}</div>
                      </FormGroup>  

                      <div className="float-right">
                        <Button color="primary" type="submit"  disabled={isSubmitting}>Update</Button>{'  '}
                      </div>
                    </Form>
                    )}
                  </Formik>
                </CardBody>
              </Card>
            </Col>
          </Container>
        </div>
      );
    }

   


  }

  const mapStateToProps = (state) => {
    return {
        uid: state.uid,
        userName: state.userName
    }
  };


export default connect(mapStateToProps, null)(ProfilePage);