
import React from 'react';
import configData from "../config/manifest.json";
import {
  withRouter,
} from "react-router-dom";
import { Container , Alert , Card  , CardBody  , Button , CardImg  , CardSubtitle , CardTitle , CardColumns } from 'reactstrap';

class FavourtePage extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        onloadData : false,
        products : [] ,
      }
    }

    componentDidMount(){
      fetch(
        configData.server_url+"/favorite", 
        {
            method: 'GET', 
            credentials: 'include',
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        }
      ).then(res => res.json())
      .then((result) => {
            if(result.status === "S"){
              this.setState({
                products : result.favorite,
                onloadData : true,
              })
            }else if(result.status === "U"){
              alert(result.message);
              this.setState({
                onloadData : true,
              })
            }
        },
            (error) => {
                alert(error);
                this.setState({
                  onloadData : true,
                })
            }
        )
    }
   
    render() {
      return (
        <div>
          <Container>
            <Alert color="light">
              <h1>Favourte</h1>
            </Alert>
          </Container>
          <br/>

          <Container>
            {this.state.onloadData ? 
              <CardColumns>
                {
                  this.state.products === [] ? <></> : 
                    this.state.products.map( (product) => {
                      return (
                        <Card>
                          <CardImg top width="100%" src={configData.server_url+"/colthImage/"+product.image} alt="Card image cap" />
                          <CardBody>
                            <CardTitle tag="h5">{product.productName}</CardTitle>
                            <CardSubtitle tag="h6" className="mb-2 text-muted">HKD$ {product.price}</CardSubtitle>
                            <Button onClick={(e) => this.onClickView(e,product)} color="info">View</Button>{'  '}
                          </CardBody>
                        </Card>
                      )
                    })
                }
              </CardColumns>
            :
            <div className="text-center">
              <div className="spinner-border" role="status">
                <span className="sr-only" >Loading...</span>
              </div>
            </div>
            }  
          </Container>

        </div>
      );
    }

    onClickView = (e,product) => {
      this.props.history.push('/product',{
        _id : product.productId,
        productName : product.productName,
        content : product.content,
        price : product.price ,
        image : product.image,
      });
    }



  }


  export default withRouter(FavourtePage);