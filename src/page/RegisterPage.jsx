
import React  from 'react';
import {
  FormGroup,
  Input,
  Col,
  Label,
  Container,
  Card,
  CardBody,
  Button,
} from "reactstrap";
import { Formik , Form } from 'formik';
import configData from "../config/manifest.json";
import {
  Link,
  withRouter,
} from "react-router-dom";

class RegisterPage extends React.Component {


   
    render() {
      return (
          <div>
          <br/><br/>
          <Container fluid="sm">
            <Col sm="12" md={{ size: 8, offset: 2 }}>
              <Card >
                <CardBody>
                  <Formik
                    initialValues={{ userName: '', password: '' }}
                    validate={values => {
                      const errors = {};
                      
                      if(values.userName === '' || values.userName === undefined){
                        errors.userName = 'Please input the user name.'
                      }

                      if(values.password === '' || values.password === undefined){
                        errors.password = 'Please input the password.'
                      }

                      return errors;
                    }}
                    onSubmit={(values, { setSubmitting }) => {
                    //   setTimeout(() => {
                    //     alert(JSON.stringify(values, null, 2));
                    //     setSubmitting(false);
                    //   }, 400);
                      fetch(
                          configData.server_url+"/register", 
                          {
                              method: 'POST', // or 'PUT'
                              body: JSON.stringify(values), // data can be `string` or {object}!
                              credentials: 'include',
                              headers: new Headers({
                                  'Content-Type': 'application/json'
                              })
                          }
                      ).then(res => res.json())
                      .then((result) => {
                            alert(result.message);
                            if(result.status === "S"){
                              this.props.history.push('/login');
                            }
                            setSubmitting(false);
                        },
                            (error) => {
                                alert(error);
                                setSubmitting(false);
                            }
                        )
                    }}

                  >{({ values, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting, }) => (
                    <Form> 
                      <h1 className="mb-3">Registration</h1>
                      <FormGroup>
                        <Label for="userName">User Name</Label>
                        <Input type="text" name="userName" onChange={handleChange} onBlur={handleBlur} value={values.userName} placeholder="UserName" />
                        <div style={{color:'red'}}>{errors.userName && touched.userName && errors.userName}</div>
                      </FormGroup>
                      
                      <FormGroup>
                        <Label for="password">Password</Label>
                        <Input type="password" name="password" onChange={handleChange} onBlur={handleBlur} value={values.password} placeholder="Password " />
                        <div style={{color:'red'}}>{errors.password && touched.password && errors.password}</div>
                      </FormGroup>  
                      <div className="float-right">
                        <Button color="primary" type="submit"  disabled={isSubmitting}>Register</Button>{'  '}
                        <Link to="/login" className="btn btn-secondary">Back</Link>
                      </div>
                    </Form>
                    )}
                  </Formik>
                </CardBody>
              </Card>
            </Col>
          </Container>
        </div>
      );
    }


    
  }


  export default withRouter(RegisterPage);