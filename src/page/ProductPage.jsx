
import React from 'react';
import { Container , Alert , Card , Label , Input , FormGroup, CardImg , Button , Col , Row } from 'reactstrap';
import configData from "../config/manifest.json";
import {
  withRouter,
} from "react-router-dom";
import { connect } from 'react-redux';

class ProductPage extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        product : {
          _id : '',
          productName : '',
          content : '',
          price : '' ,
          image : '',
        },
        qut : 1,
        bt_po : false,
        bt_fav : false,
        bt_isAddFav : false,
      }
    }

    componentDidMount(){
      var plink = encodeURI(configData.server_url+"/productById/"+this.props.location.state._id);
      var flink = encodeURI(configData.server_url+"/favorite/"+this.props.location.state._id);

      fetch(
        plink, 
        {
            method: 'GET', 
            credentials: 'include',
        }
      ).then(res => res.json())
      .then((result) => {
            if(result.status === "S"){
              if(result.product !== {} && result.product !== undefined && result.product !== null){
                this.setState({
                  product : result.product,
                });
              }else{
                // alert("The product have not exist.");
                // this.props.history.goBack();
              }
              
            }
        },
            (error) => {
                alert(error);
            }
        );

        fetch(
          flink, 
          {
              method: 'GET', 
              credentials: 'include',
          }
        ).then(res => res.json())
        .then((result) => {

              if(result.status === "S"){
                this.setState({
                  bt_isAddFav : !this.state.bt_isAddFav,
                })
              }

            }
          ,
              (error) => {
                  alert(error);
              }
          );

    }
   
    render() {
      return (
        <div>
          <Container>
            <Alert color="light">
              <h1>{this.state.product.productName}</h1>
            </Alert>
          </Container>
          
          <Container>
            <Row xs="2">
              <Col>
                <Card inverse>
                  <CardImg width="100%" src={configData.server_url+"/colthImage/"+this.state.product.image} alt="Card image cap" />
                </Card>
              </Col>
              <Col>
                <Row>
                  <h3>Content </h3>
                  <p>{this.state.product.content}</p>
                  <b>Price : HKD$ {this.state.product.price}</b>
                </Row>
                <Row>
                  <Container>
                    <Alert color="dark">
                      <Col sm={6}>
                        <FormGroup>
                          <Label for="exampleNumber"><b>Quantity</b></Label>
                          <Input
                            type="number"
                            name="number"
                            id="exampleNumber"
                            placeholder="Quantity"
                            value={this.state.qut}
                            min={1}
                            max={100}
                            onChange = {(e)=>{
                              this.setState({
                                qut : e.target.value
                              })
                            }}
                          />
                          <br/>
                          <Button disabled={this.state.bt_po} onClick={() => this.onClickPO()}>Place Order</Button>{'  '}
                        </FormGroup>
                      </Col>
                      
                    </Alert>
                    <br/>
                    <br/>
                    <div className="float-right">
                      {this.state.bt_isAddFav ?
                        <><Button disabled={this.state.bt_fav} color="success" onClick={() => this.onClickDelFav()}>Remove favorite</Button>{'  '}</>
                        :
                        <><Button disabled={this.state.bt_fav} color="success" onClick={() => this.onClickAddFav()}>Add favorite</Button>{'  '}</>
                      }
                      <Button onClick={() => { this.props.history.goBack(); }}>Back</Button>{'  '}
                    </div>
                  </Container>
                </Row>
              </Col>
            </Row>
          </Container>
        </div>
      );
    }

    onClickPO = () =>{

      this.setState({
        bt_po : !this.state.bt_po
      });

      var product = {
        productName : this.state.product.productName,
        price : this.state.product.price,
        quantity : this.state.qut,
        image : this.state.product.image,
      }

      fetch(
        configData.server_url+"/createOrder", 
        {
            method: 'POST', 
            body: JSON.stringify(product), 
            credentials: 'include',
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        }
      ).then(res => res.json())
      .then((result) => {
          alert(result.message);
          this.setState({
            bt_po : !this.state.bt_po
          });
        },
            (error) => {
                alert(error);
                this.setState({
                  bt_po : !this.state.bt_po
                });
            }
        )

      

    }

    onClickAddFav = () =>{
      this.setState({
        bt_fav : !this.state.bt_fav
      });

      var favorite = {
        productId : this.state.product._id,
        productName : this.state.product.productName,
        price : this.state.product.price,
        image : this.state.product.image,
      }

      fetch(
        configData.server_url+"/createfavorite", 
        {
            method: 'POST', 
            body: JSON.stringify(favorite), 
            credentials: 'include',
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        }
      ).then(res => res.json())
      .then((result) => {
            alert(result.message);
            this.setState({
              bt_fav : !this.state.bt_fav,
              bt_isAddFav : !this.state.bt_isAddFav
            });
        },
            (error) => {
                alert(error);
                this.setState({
                  bt_fav : !this.state.bt_fav
                });
            }
        )
    }


  onClickDelFav = () =>{
      this.setState({
        bt_fav : !this.state.bt_fav
      });


      fetch(
        configData.server_url+"/favorite/"+this.state.product._id, 
        {
            method: 'DELETE', 
            credentials: 'include',
        }
      ).then(res => res.json())
      .then((result) => {
            alert(result.message);
            this.setState({
              bt_fav : !this.state.bt_fav,
              bt_isAddFav : !this.state.bt_isAddFav
            });
        },
            (error) => {
                alert(error);
                this.setState({
                  bt_fav : !this.state.bt_fav
                });
            }
        )
    }

  }


  const mapStateToProps = (state) => {
    return {
        uid: state.uid,
        userName: state.userName
    }
  };


export default connect(mapStateToProps, null)(withRouter(ProductPage));