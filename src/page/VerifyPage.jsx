
import React from 'react';
import configData from "../config/manifest.json";
import {
  withRouter,
} from "react-router-dom";
import { connect } from 'react-redux';
import {getUserstatus , setUserstatus} from '../redux/actions';


class VerifyPage extends React.Component {

    componentDidMount(){
        fetch(
            configData.server_url+"/loginStatus", 
            {
                method: 'GET', 
                credentials: 'include',
                headers: new Headers({
                    'Content-Type': 'application/json'
                })
            }
          ).then(res => {
            if(res.ok) {
              return res.json();
            }
      
            throw new Error('');
      
          })
          .then((result) => {
                if(result !== null && result !== undefined){
                  this.props.SET_USER({
                    uid : result.uid,
                    userName : result.userName
                  });
      
                  this.props.history.push('/home');
                }
            },
                (error) => {
                    this.props.history.push('/login');
                }
            )
    }
    
   
    render() {
      return (
            <>
            </>
      );
    }
  }


  
  const mapDispatchToProps = (dispatch, ownProps) => {
  
    return ({
      SET_USER: (user) => dispatch(setUserstatus(user)),
      GET_USER: () => dispatch(getUserstatus()),
    });
  }


  export default connect(null, mapDispatchToProps)(withRouter(VerifyPage));