
import React from 'react';
import configData from "../config/manifest.json";
import {
  withRouter,
} from "react-router-dom";
import { Container , Alert , Button  , Input , Label , Col , Row , CardImg , Card, CardBody, CardText, CardTitle } from 'reactstrap';

class OrderPage extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        orders : [],
        onloadData : false,
        totalPrice : 0,
      }
    }

    componentDidMount(){


      fetch(
        configData.server_url+"/order", 
        {
            method: 'GET', // or 'PUT'
            credentials: 'include',
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        }
      ).then(res => res.json())
      .then((result) => {
            if(result.status === "S"){
              var totp = 0;
              for(var n = 0 ; n < result.orders.length ; n++){
                totp = totp + ( result.orders[n].quantity * result.orders[n].price )
              }
              this.setState({
                orders : result.orders,
                onloadData : true,
                totalPrice : totp,
              })
            }else if(result.status === "U"){
              alert(result.message);
              this.setState({
                onloadData : true,
              })
            }
        },
            (error) => {
                alert(error);
                this.setState({
                  onloadData : true,
                })
            }
        )

        console.log(this.state.orders)
    }
   
    render() {
      return (
        <div>
          <Container>
            <Alert color="light">
              <h1>Order</h1>
            </Alert>
          </Container>
          <Container>
            
                {this.state.onloadData ? 
                  this.state.orders.length !== 0 && this.state.orders !== undefined && this.state.orders !== null?
                    this.state.orders.map( (order,i) =>
                      <>
                        <Card>
                          <CardBody>
                            <Row>
                              <Col sm={3}>
                                <CardImg top src={configData.server_url+"/colthImage/"+order.image} style={{height: '90%' , width: '70%'}} />
                              </Col>
                              <Col sm={9}>
                                  <CardTitle>{order.productName}</CardTitle>
                                  <CardText>HKD $ {order.price}</CardText>
                                  <br/>
                                  <div className="float-right">
                                    <Label for="exampleNumber"><b>Quantity</b></Label>
                                    <Input
                                      type="number"
                                      name="number"
                                      id="exampleNumber"
                                      placeholder="Quantity"
                                      value={this.state.orders[i].quantity}
                                      min={1}
                                      max={100}
                                      onChange = {(e)=>{

                                        const newItems = [...this.state.orders];
                                        newItems[i].quantity = e.target.value;
                                        this.setState({ orders:newItems });

                                      }}
                                    />
                                    <br/>
                                    <Button color="primary" type="button" onClick={(e) => this.onClickUpdate(e,i)} >update</Button>{'   '}
                                    <Button color="danger" type="button"  onClick={(e) => this.onClickDelete(e,i)} >delete</Button>
                                  </div>
                              </Col>
                            </Row>
                          </CardBody>
                        </Card>
                      </>
                    )
                    :
                    <></>
                  :
                  <div className="text-center">
                    <div className="spinner-border" role="status">
                      <span className="sr-only" >Loading...</span>
                    </div>
                  </div>
                }
                {this.state.onloadData ? 
                    this.state.orders.length !== 0 && this.state.orders !== undefined && this.state.orders !== null?
                    <Alert color="dark" className="text-right">
                      <p><b>Total Price : {this.state.totalPrice}</b></p>
                      <div className="floating-right">
                        <Button color="success" type="button"  onClick={(e) => this.onClickPurchase()} >Purchase</Button>
                      </div>
                    </Alert>
                    :
                    <></>
                  :
                  <></>
                }
                

            
          </Container>
        </div>
      );
    }

    onClickUpdate = (e,i) => {
      this.setState({
        onloadData : !this.state.onloadData
      })
      fetch(
        configData.server_url+"/order/"+this.state.orders[i]._id, 
        {
            method: 'PUT', // or 'PUT'
            body: JSON.stringify(this.state.orders[i]), // data can be `string` or {object}!
            credentials: 'include',
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        }
      ).then(res => res.json())
      .then((result) => {
          alert(result.message);
          var totalPrice = 0;
          for(var n = 0 ; n < this.state.orders.length ; n++){
            totalPrice = totalPrice + ( this.state.orders[n].quantity * this.state.orders[n].price )
          }
          this.setState({
            onloadData : !this.state.onloadData,
            totalPrice : totalPrice,
          })
        },
            (error) => {
                alert(error);
            }
        )


    }

    onClickDelete = (e,i) => {
      this.setState({
        onloadData : !this.state.onloadData
      })
      fetch(
        configData.server_url+"/order/"+this.state.orders[i]._id, 
        {
            method: 'DELETE', // or 'PUT'
            credentials: 'include',
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        }
      ).then(res => res.json())
      .then((result) => {
          alert(result.message);
          if(result.status === 'S'){
            var totalPrice = this.state.totalPrice - ( this.state.orders[i].quantity * this.state.orders[i].price );
            let filteredArray = this.state.orders.filter(item => item._id !== this.state.orders[i]._id)
            this.setState({orders: filteredArray , totalPrice : totalPrice , onloadData : !this.state.onloadData});
          }
            
        },
            (error) => {
                alert(error);
            }
        )
    }

    onClickPurchase = () =>{
      this.setState({
        onloadData : !this.state.onloadData
      })
      fetch(
        configData.server_url+"/order", 
        {
            method: 'DELETE', // or 'PUT'
            credentials: 'include',
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        }
      ).then(res => res.json())
      .then((result) => {
          alert(result.message);
          if(result.status === 'S'){
            this.setState({orders: [] , totalPrice : 0 , onloadData : !this.state.onloadData});
          }
        },
            (error) => {
                alert(error);
            }
        )
    }


  }


export default withRouter(OrderPage);